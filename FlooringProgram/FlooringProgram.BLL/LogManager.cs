﻿using System.IO;

namespace FlooringProgram.BLL
{
    public static class LogManager
    {
        private static string _filePath = @"C:\_Repos\erik.feeley.self.work\FlooringProgram\FlooringProgram.Tests\DataFiles" + @"\Logs.txt";

        public static void LogError(string message)
        {
            using (var sr = new StreamWriter(_filePath, true))
            {
                sr.WriteLine(message);
            }
        }
    }
}
