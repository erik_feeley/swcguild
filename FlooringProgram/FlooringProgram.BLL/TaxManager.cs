﻿using System;
using System.Collections.Generic;
using FlooringProgram.Data.Factories;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.BLL
{
    public class TaxManager
    {
        private ITaxRepository _taxRepository;

        public TaxManager()
        {
            _taxRepository = TaxRepositoryFactory.GetRepository();
        }

        public Response<List<Tax>> GetStates()
        {
            var taxResponse = new Response<List<Tax>>();

            try
            {
                taxResponse.Data = _taxRepository.GetStates();
                taxResponse.Message = "Here are all the states we service";
                taxResponse.Success = true;
            }
            catch (Exception exception)
            {
                taxResponse.Success = false;
                taxResponse.Message = "There was an error please try again later";
                LogManager.LogError(exception.Message);
            }
            return taxResponse;
        }
    }
}
