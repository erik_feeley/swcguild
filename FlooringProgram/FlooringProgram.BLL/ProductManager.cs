﻿using System;
using System.Collections.Generic;
using FlooringProgram.Data.Factories;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.BLL
{
    public class ProductManager
    {
        private IProductRepository _productRepository;

        public ProductManager()
        {
            _productRepository = ProductRepositoryFactory.GetRepository();
        }

        public Response<List<Product>> GetProducts()
        {
            var productResponse = new Response<List<Product>>();

            try
            {
                productResponse.Data = _productRepository.GetProducts();
                productResponse.Message = "Here are the products we work with";
                productResponse.Success = true;
            }
            catch (Exception exception)
            {
                productResponse.Success = false;
                productResponse.Message = "There was an error please try again later";
                LogManager.LogError(exception.Message);
            }
            return productResponse;
        }
    }
}
