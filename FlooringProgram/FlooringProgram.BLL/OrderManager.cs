﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlooringProgram.Data.Factories;
using FlooringProgram.Models;
using FlooringProgram.Models.Enums;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.BLL
{
    public class OrderManager
    {
        private IOrderRepository _orderRepository;

        public OrderManager()
        {
            _orderRepository = OrderRepositoryFactory.GetRepository();
        }

        public List<string> GetDates()
        {
            return _orderRepository.GetDates();
        }

        public List<int> GetValidOrderNumbers(string date)
        {
            return _orderRepository.GetOrderNumbers(date);
        }

        public Response<List<Order>> GetOrdersByDate(string date)
        {
            var response = new Response<List<Order>>();
            try
            {
                if (!_orderRepository.OrderDates.Contains(date))
                {
                    response.Message = "\n\nThat date does not exist!";
                    response.Success = false;
                }
                else
                {
                    response.Message = $"Here are the orders for the date {date}\n";
                    response.Success = true;
                    response.Data = _orderRepository.GetOrdersByDate(date).Where(o => o.Status != StatusType.Deleted
                                    && o.Status != StatusType.Moved).ToList();
                }
            }
            catch (Exception exception)
            {
                response.Success = false;
                response.Message = "There was an error. Please try again later.";
                LogManager.LogError(exception.Message);
            }
            return response;
        }

        public Response<Order> CreateOrder(string date, Order order)
        {
            var response = new Response<Order>();
            try
            {
                _orderRepository.AddOrder(date, order);
                response.Success = true;
                response.Message = "\nOrder added.\n";
                response.Data = order;
            }
            catch (Exception exception)
            {
                response.Success = false;
                response.Message = "There was an error. Please try again later.";
                LogManager.LogError(exception.Message);
            }
            return response;
        }

        public bool DeleteOrder(string date, int orderNumber)
        {
            try
            {
                _orderRepository.DeleteOrder(date, orderNumber);
            }
            catch (Exception exception)
            {
                LogManager.LogError(exception.Message);
                return false;
            }
            return true;
        }

        public bool MoveOrder(string date, int orderNumber)
        {
            try
            {
                _orderRepository.MoveOrder(date, orderNumber);
            }
            catch (Exception exception)
            {
                LogManager.LogError(exception.Message);
                return false;
            }
            return true;
        }

        public Response<Order> DoesOrderExist(string date, int orderNumber)
        {
            var response = new Response<Order>();

            try
            {
                var orders = GetOrdersByDate(date);
                if (orders.Success && orders.Data.Any(o => o.OrderNumber == orderNumber))
                {
                    response.Success = true;
                    response.Message = "\nOrder exists and has been loaded.\n";
                    response.Data = orders.Data.Find(o => o.OrderNumber == orderNumber);
                }
                else
                {
                    response.Success = false;
                    response.Message = "\nOrder does not exist or is not active.\n";
                    response.Data = null;
                }
            }
            catch (Exception exception)
            {
                response.Success = false;
                response.Message = "An error occured";
                LogManager.LogError(exception.Message);
            }
            return response;
        }

        public void UpdateOrder(int orderNumber, string date, Order updatedOrder)
        {
            try
            {
                var orders = GetOrdersByDate(date);
                var newOrderToInsert = updatedOrder.Clone() as Order; // better to this or cast with (Order)
                if (newOrderToInsert != null)
                {
                    newOrderToInsert.Status = StatusType.Active;
                    if (orders.Success == false)
                    {
                        orders.Data = new List<Order>();
                        newOrderToInsert.OrderNumber = 1;
                        orders.Data.Add(newOrderToInsert);
                        _orderRepository.UpdateOrder(date, orders.Data);
                        return;
                    }
                    if (updatedOrder.Status == StatusType.Moved)
                    {
                        newOrderToInsert.OrderNumber = GetTrueCount(date) + 1;
                        orders.Data.Add(newOrderToInsert);
                        _orderRepository.UpdateOrder(date, orders.Data);
                        return;
                    }
                    var index = orders.Data.FindIndex(o => o.OrderNumber == orderNumber);
                    orders.Data.RemoveAt(index);
                    orders.Data.Insert(index, newOrderToInsert);
                }
                _orderRepository.UpdateOrder(date, orders.Data);
            }
            catch (Exception exception)
            {
                LogManager.LogError(exception.Message);
            }
            
        }

        private int GetTrueCount(string date)
        {
            return _orderRepository.GetOrdersByDate(date).Count;
        }

        public void MapInfoToOrder(Product newProduct, Tax newTaxInfo, Order order)
        {
            order.State = newTaxInfo.StateName;
            order.TaxRate = newTaxInfo.TaxRate;
            order.ProductType = newProduct.ProductType;
            order.CostPerSquareFoot = newProduct.CostPerSquareFoot;
            order.LaborCostPerSquareFoot = newProduct.LaborCostPerSquareFoot;
        }
    }
}
