﻿using System.Collections.Generic;
using System.Linq;
using FlooringProgram.Models;
using FlooringProgram.Models.Enums;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.TestRepositories
{
    public class MockOrderRepository : IOrderRepository
    {
        public static List<Order> Orders { get; set; }
        public List<string> OrderDates { get; set; }
        public static Dictionary<string, List<Order>> OrdersByDate = new Dictionary<string, List<Order>>
        {
            { "06012013", new List<Order>
            {
                new Order { OrderNumber = 1, CustomerName = "derpington", State = "ohio", TaxRate =  6.25m / 100, ProductType = "carpet", Area = 30m, CostPerSquareFoot = 2.25m, LaborCostPerSquareFoot = 2.10m },
                new Order { OrderNumber = 2, CustomerName = "flurtfurp", State = "michigan", TaxRate =  5.75m / 100, ProductType = "tile", Area = 30m, CostPerSquareFoot = 3.50m, LaborCostPerSquareFoot = 4.15m }
            } },
            { "06022013", new List<Order>
            {
                new Order { OrderNumber = 1, CustomerName = "derpington", State = "ohio", TaxRate =  6.25m / 100, ProductType = "carpet", Area = 30m, CostPerSquareFoot = 2.25m, LaborCostPerSquareFoot = 2.10m },
                new Order { OrderNumber = 2, CustomerName = "flurtfurp", State = "michigan", TaxRate =  5.75m / 100, ProductType = "tile", Area = 30m, CostPerSquareFoot = 3.50m, LaborCostPerSquareFoot = 4.15m }
            } }
        };

        public MockOrderRepository()
        {
            GetOrderDates();
        }

        private void GetOrderDates()
        {
            OrderDates = OrdersByDate.Keys.ToList();
        }

        public List<string> GetDates()
        {
            return OrderDates;
        }

        public List<int> GetOrderNumbers(string date)
        {
            return (OrdersByDate[date].Where(order => order.Status == StatusType.Active)
                                      .Select(order => order.OrderNumber)).ToList();
        }

        public void AddOrder(string date, Order newOrder)
        {
            if (OrdersByDate.ContainsKey(date))
            {
                newOrder.OrderNumber = OrdersByDate[date].Count + 1;
                OrdersByDate[date].Add(newOrder);
            }
            else
            {
                OrderDates.Add(date);
                newOrder.OrderNumber = 1;
                OrdersByDate.Add(date, new List<Order> { newOrder });
            }
        }

        public bool DeleteOrder(string date, int orderNumber)
        {
            OrdersByDate[date].Find(o => o.OrderNumber == orderNumber).Status = StatusType.Deleted;
            return true;
        }

        public void UpdateOrder(string date, List<Order> orders)
        {
            OrdersByDate[date] = orders;
        }

        public List<Order> GetOrdersByDate(string date)
        {
            return OrdersByDate[date];
        }

        public void MoveOrder(string date, int orderNumber)
        {
            OrdersByDate[date].Find(o => o.OrderNumber == orderNumber).Status = StatusType.Moved;
        }
    }
}
