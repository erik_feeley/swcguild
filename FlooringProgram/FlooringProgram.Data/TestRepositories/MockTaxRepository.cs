﻿using System.Collections.Generic;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.TestRepositories
{
    public class MockTaxRepository : ITaxRepository
    {
        public List<Tax> Taxes { get; set; }

        public MockTaxRepository()
        {
            LoadTaxInfo();
        }

        private void LoadTaxInfo()
        {
            Taxes = new List<Tax>
            {
                new Tax { StateAbbreviation = "oh", StateName = "ohio", TaxRate  = 6.25m / 100 },
                new Tax { StateAbbreviation = "pa", StateName = "pennsylvania", TaxRate  = 6.75m / 100 },
                new Tax { StateAbbreviation = "mi", StateName = "michigan", TaxRate  = 5.75m / 100 },
                new Tax { StateAbbreviation = "in", StateName = "indiana", TaxRate  = 6.00m / 100 }
            };
        }

        public List<Tax> GetStates()
        {
            return Taxes;
        }
    }
}
