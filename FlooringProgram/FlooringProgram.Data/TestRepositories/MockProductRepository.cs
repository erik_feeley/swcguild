﻿using System.Collections.Generic;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.TestRepositories
{
    public class MockProductRepository : IProductRepository
    {
        public List<Product> Products { get; set; }

        public MockProductRepository()
        {
            LoadProducts();
        }

        private void LoadProducts()
        {
            Products = new List<Product>
            {
                new Product {ProductType = "carpet", CostPerSquareFoot = 2.25m, LaborCostPerSquareFoot = 2.10m },
                new Product {ProductType = "laminate", CostPerSquareFoot = 1.75m, LaborCostPerSquareFoot = 2.10m },
                new Product {ProductType = "tile", CostPerSquareFoot = 3.50m, LaborCostPerSquareFoot = 4.15m },
                new Product {ProductType = "wood", CostPerSquareFoot = 5.15m, LaborCostPerSquareFoot = 4.75m }
            };
        }

        public List<Product> GetProducts()
        {
            return Products;
        }
    }
}
