﻿using FlooringProgram.Models;

namespace FlooringProgram.Data.ObjectMappers
{
    public static class TaxMapper
    {
        public static Tax MapToTax(string tax)
        {
            var tmp = new Tax();
            var fields = tax.Split(',');
            tmp.StateAbbreviation = fields[0];
            tmp.StateName = fields[1];
            tmp.TaxRate = decimal.Parse(fields[2]) / 100;
            return tmp;
        }
    }
}
