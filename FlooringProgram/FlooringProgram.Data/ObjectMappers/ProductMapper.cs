﻿using FlooringProgram.Models;

namespace FlooringProgram.Data.ObjectMappers
{
    public static class ProductMapper
    {
        public static Product MapToProduct(string product)
        {
            var tmp = new Product();
            var fields = product.Split(',');
            tmp.ProductType = fields[0];
            tmp.CostPerSquareFoot = decimal.Parse(fields[1]);
            tmp.LaborCostPerSquareFoot = decimal.Parse(fields[2]);
            return tmp;
        }
    }
}
