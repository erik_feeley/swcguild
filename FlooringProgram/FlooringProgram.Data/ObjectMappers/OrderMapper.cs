﻿using System;
using FlooringProgram.Models;
using FlooringProgram.Models.Enums;

namespace FlooringProgram.Data.ObjectMappers
{
    public static class OrderMapper
    {
        private const string Delimiter = "|||";

        public static Order MapToOrder(string order)
        {
            var tmp = new Order();
            var fields = order.Split(',');
            tmp.OrderNumber = int.Parse(fields[0]);
            if (fields[1].Contains("|||"))
                fields[1] = fields[1].Replace(Delimiter, ","); 
            tmp.CustomerName = fields[1];
            tmp.State = fields[2];
            tmp.TaxRate = decimal.Parse(fields[3]);
            tmp.ProductType = fields[4];
            tmp.Area = decimal.Parse(fields[5]);
            tmp.CostPerSquareFoot = decimal.Parse(fields[6]);
            tmp.LaborCostPerSquareFoot = decimal.Parse(fields[7]);
            tmp.Status = (StatusType) Enum.Parse(typeof(StatusType), fields[8]);
            return tmp;
        }
    }
}
