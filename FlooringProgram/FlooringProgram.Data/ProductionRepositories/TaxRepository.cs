﻿using System.Collections.Generic;
using System.IO;
using FlooringProgram.Data.ObjectMappers;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.ProductionRepositories
{
    public class TaxRepository : ITaxRepository
    {
        //private static string _filePath = @"C:\_repos\erik.feeley.self.work\FlooringProgram\FlooringProgram.UI\DataFiles\StateFiles\States.txt";
        private static string _filePath = @"~\DataFiles\StateFiles\States.txt";
        public List<Tax> Taxes { get; set; }

        public TaxRepository()
        {
            LoadTaxInfo();
        }

        private void LoadTaxInfo()
        {
            Taxes = new List<Tax>();
            using (var sr = new StreamReader(_filePath))
            {
                sr.ReadLine(); // skip first line
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    var tax = TaxMapper.MapToTax(line);
                    Taxes.Add(tax);
                }
            }
        }

        public List<Tax> GetStates()
        {
            return Taxes;
        }
    }
}
