﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FlooringProgram.Data.ObjectMappers;
using FlooringProgram.Models;
using FlooringProgram.Models.Enums;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.ProductionRepositories
{
    public class OrderRepository : IOrderRepository
    {
        //private static string _filePath = @"C:\_Repos\erik.feeley.self.work\FlooringProgram\FlooringProgram.UI\DataFiles\OrderFiles";
        private static string _filePath = @"~\DataFiles\OrderFiles";
        public List<Order> Orders { get; set; }
        public List<string> OrderDates { get; set; }

        public OrderRepository()
        {
            GetOrderDates();
        }

        private void GetOrderDates()
        {
            OrderDates = new List<string>();
            foreach (var file in Directory.GetFiles(_filePath))
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file);
                if (fileNameWithoutExtension != null)
                    OrderDates.Add(fileNameWithoutExtension.Remove(0, 7));
            }
        }

        private List<Order> LoadOrders(string date)
        {
            Orders = new List<Order>();
            var fileName = "Orders_" + date + ".txt";
            using (var sr = new StreamReader(_filePath + fileName))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    var order = OrderMapper.MapToOrder(line);
                    Orders.Add(order);
                }
            }
            return Orders;
        }

        private void SaveOrders(string path, List<Order> orders)
        {
            using (var sw = new StreamWriter(path))
            {
                foreach (var order in orders)
                {
                    sw.WriteLine(order.ToString());
                }
            }
        }

        public List<Order> GetOrdersByDate(string date)
        {
            return LoadOrders(date);
        }

        public List<string> GetDates()
        {
            return OrderDates;
        }

        public List<int> GetOrderNumbers(string date)
        {
            var orders = GetOrdersByDate(date);
            return (orders.Where(order => order.Status == StatusType.Active).Select(order => order.OrderNumber)).ToList();
        }

        public void AddOrder(string date, Order newOrder)
        {
            var path = _filePath + @"\Orders_" + date + ".txt";

            if (newOrder.CustomerName.Contains(','))
                newOrder.CustomerName = newOrder.CustomerName.Replace(",", "|||");

            List<Order> ordersToSave;
            if (OrderDates.Contains(date))
            {
                ordersToSave = GetOrdersByDate(date);
                newOrder.OrderNumber = ordersToSave.Count + 1;
                ordersToSave.Add(newOrder);
            }
            else
            {
                ordersToSave = new List<Order>();
                newOrder.OrderNumber = 1;
                ordersToSave.Add(newOrder);
            }
            SaveOrders(path, ordersToSave);
        }

        public bool DeleteOrder(string date, int orderNumber)
        {
            var path = _filePath + @"\Orders_" + date + ".txt";
            var orders = GetOrdersByDate(date);
            orders.Find(o => o.OrderNumber == orderNumber).Status = StatusType.Deleted;
            SaveOrders(path, orders);
            return true;
        }

        public void MoveOrder(string date, int orderNumber)
        {
            var path = _filePath + @"\Orders_" + date + ".txt";
            var orders = GetOrdersByDate(date);
            orders.Find(o => o.OrderNumber == orderNumber).Status = StatusType.Moved;
            SaveOrders(path, orders);
        }

        public void UpdateOrder(string date, List<Order> orders)
        {
            var path = _filePath + @"\Orders_" + date + ".txt";
            SaveOrders(path, orders);
        }
    }
}
