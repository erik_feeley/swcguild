﻿using System.Collections.Generic;
using System.IO;
using FlooringProgram.Data.ObjectMappers;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.ProductionRepositories
{
    public class ProductRepository : IProductRepository
    {
        //private static string _filePath = @"C:\_Repos\erik.feeley.self.work\FlooringProgram\FlooringProgram.UI\DataFiles\ProductFiles\Products.txt";
        private static string _filePath = @"~\DataFiles\ProductFiles\Products.txt";
        public List<Product> Products { get; set; }

        public ProductRepository()
        {
            LoadProducts();
        }

        private void LoadProducts()
        {
            Products = new List<Product>();
            using (var sr = new StreamReader(_filePath))
            {
                sr.ReadLine(); // skip first line
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    var product = ProductMapper.MapToProduct(line);
                    Products.Add(product);
                }
            }
        }

        public List<Product> GetProducts()
        {
            return Products;
        }
    }
}
