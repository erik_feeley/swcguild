﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.XMLRepositories
{
    public class XmlProductRepository : IProductRepository
    {
        private static string _filePath = @"~\DataFiles\XMLProductFiles\Products.xml";
        public List<Product> Products { get; set; }

        public XmlProductRepository()
        {
            LoadProducts();
        }

        private void LoadProducts()
        {
            Products = new List<Product>();
            var reader = new XmlSerializer(typeof(List<Product>));
            using (var file = new StreamReader(_filePath))
            {
                Products = (List<Product>) reader.Deserialize(file);
            }
        }

        public List<Product> GetProducts()
        {
            return Products;
        }
    }
}
