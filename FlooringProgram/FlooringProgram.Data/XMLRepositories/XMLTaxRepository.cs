﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.XMLRepositories
{
    public class XmlTaxRepository : ITaxRepository
    {
        private static string _filePath = @"~\DataFiles\XMLStateFiles\States.xml";
        public List<Tax> Taxes { get; set; }

        public XmlTaxRepository()
        {
            LoadTaxInfo();
        }

        private void LoadTaxInfo()
        {
            Taxes = new List<Tax>();
            var reader = new XmlSerializer(typeof(List<Tax>));
            using (var file = new StreamReader(_filePath))
            {
                Taxes = (List<Tax>) reader.Deserialize(file);
            }
        }

        public List<Tax> GetStates()
        {
            return Taxes;
        }
    }
}
