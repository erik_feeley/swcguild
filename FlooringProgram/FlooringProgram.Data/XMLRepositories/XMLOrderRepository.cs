﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using FlooringProgram.Models;
using FlooringProgram.Models.Enums;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.XMLRepositories
{
    public class XmlOrderRepository : IOrderRepository
    {
        private static string _filePath = @"~\DataFiles\XMLOrderFiles";
        public List<Order> Orders { get; set; }
        public List<string> OrderDates { get; set; }

        public XmlOrderRepository()
        {
            GetOrderDates();
        }

        private void GetOrderDates()
        {
            OrderDates = new List<string>();
            foreach (var file in Directory.GetFiles(_filePath))
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file);
                if (fileNameWithoutExtension != null)
                    OrderDates.Add(fileNameWithoutExtension.Remove(0, 7));
            }
        }

        private List<Order> LoadOrders(string date)
        {
            Orders = new List<Order>();
            var fileName = @"\Orders_" + date + ".xml";
            var reader = new XmlSerializer(typeof(List<Order>));
            if (File.Exists(_filePath + fileName))
            {
                using (var file = new StreamReader(_filePath + fileName))
                {
                    Orders = (List<Order>) reader.Deserialize(file);
                }
            }
            return Orders;
        }

        private void SaveOrders(string path, List<Order> orders)
        {
            var serializer = new XmlSerializer(typeof(List<Order>));
            var fileStream = File.Create(path);
            serializer.Serialize(fileStream, orders);
            fileStream.Close();
        }

        public List<Order> GetOrdersByDate(string date)
        {
            return LoadOrders(date);
        }

        public List<string> GetDates()
        {
            return OrderDates;
        }

        public List<int> GetOrderNumbers(string date)
        {
            var orders = GetOrdersByDate(date);
            return (orders.Where(order => order.Status == StatusType.Active).Select(order => order.OrderNumber)).ToList();
        }

        public void AddOrder(string date, Order newOrder)
        {
            var path = _filePath + @"\Orders_" + date + ".xml";
            var orders = GetOrdersByDate(date);
            newOrder.OrderNumber = orders.Count + 1;
            orders.Add(newOrder);
            SaveOrders(path, orders);
        }

        public void MoveOrder(string date, int orderNumber)
        {
            var path = _filePath + @"\Orders_" + date + ".xml";
            var orders = GetOrdersByDate(date);
            orders.Find(o => o.OrderNumber == orderNumber).Status = StatusType.Moved;
            SaveOrders(path, orders);
        }

        public bool DeleteOrder(string date, int orderNumber)
        {
            var path = _filePath + @"\Orders_" + date + ".xml";
            var orders = GetOrdersByDate(date);
            orders.Find(o => o.OrderNumber == orderNumber).Status = StatusType.Deleted;
            SaveOrders(path, orders);
            return true;
        }

        public void UpdateOrder(string date, List<Order> orders)
        {
            var path = _filePath + @"\Orders_" + date + ".xml";
            SaveOrders(path, orders);
        }
    }
}
