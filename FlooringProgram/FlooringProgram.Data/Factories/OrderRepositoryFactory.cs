﻿using System.Configuration;
using FlooringProgram.Data.ProductionRepositories;
using FlooringProgram.Data.TestRepositories;
using FlooringProgram.Data.XMLRepositories;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.Factories
{
    public static class OrderRepositoryFactory
    {
        public static IOrderRepository GetRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new OrderRepository();
                case "Test":
                    return new MockOrderRepository();
                case "XML":
                    return new XmlOrderRepository();
                default:
                    return new MockOrderRepository();
            }
        }
    }
}
