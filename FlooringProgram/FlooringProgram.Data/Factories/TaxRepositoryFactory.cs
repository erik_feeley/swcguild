﻿using System.Configuration;
using FlooringProgram.Data.ProductionRepositories;
using FlooringProgram.Data.TestRepositories;
using FlooringProgram.Data.XMLRepositories;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.Factories
{
    public class TaxRepositoryFactory
    {
        public static ITaxRepository GetRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new TaxRepository();
                case "Test":
                    return new MockTaxRepository();
                case "XML":
                    return new XmlTaxRepository();
                default:
                    return new MockTaxRepository();
            }
        }
    }
}
