﻿using System.Configuration;
using FlooringProgram.Data.ProductionRepositories;
using FlooringProgram.Data.TestRepositories;
using FlooringProgram.Data.XMLRepositories;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.Factories
{
    public static class ProductRepositoryFactory
    {
        public static IProductRepository GetRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new ProductRepository();
                case "Test":
                    return new MockProductRepository();
                case "XML":
                    return new XmlProductRepository();
                default:
                    return new MockProductRepository();
            }
        }
    }
}
