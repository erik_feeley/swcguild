﻿using System;
using FlooringProgram.Models.Enums;

namespace FlooringProgram.Models
{
    [Serializable]
    public class Order : ICloneable
    {
        public int OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string State { get; set; }
        public decimal TaxRate { get; set; }
        public string ProductType { get; set; }
        public decimal Area { get; set; }
        public decimal CostPerSquareFoot { get; set; }
        public decimal LaborCostPerSquareFoot { get; set; }
        public StatusType Status { get; set; }
        public decimal MaterialCost { get { return Area * CostPerSquareFoot; } }
        public decimal LaborCost { get { return Area * LaborCostPerSquareFoot; } }
        public decimal Tax { get { return (MaterialCost + LaborCost) * TaxRate; } }
        public decimal Total { get { return MaterialCost + LaborCost + Tax; } }

        public override string ToString()
        {
            return (OrderNumber + "," + CustomerName + "," + State + "," + TaxRate + "," + ProductType + "," + Area + "," +
                 CostPerSquareFoot + "," + LaborCostPerSquareFoot + "," + (int) Status).ToLower();
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
