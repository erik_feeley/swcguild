﻿using System.Collections.Generic;

namespace FlooringProgram.Models.Interfaces
{
    public interface IProductRepository
    {
        List<Product> Products { get; set; }

        List<Product> GetProducts();
    }
}
