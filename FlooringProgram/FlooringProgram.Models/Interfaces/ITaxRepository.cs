﻿using System.Collections.Generic;

namespace FlooringProgram.Models.Interfaces
{
    public interface ITaxRepository
    {
        List<Tax> Taxes { get; set; }

        List<Tax> GetStates();
    }
}
