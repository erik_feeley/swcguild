﻿namespace FlooringProgram.Models.Interfaces
{
    public interface IWorkflow
    {
        void Execute();
    }
}
