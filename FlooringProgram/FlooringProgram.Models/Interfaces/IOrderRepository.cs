﻿using System.Collections.Generic;

namespace FlooringProgram.Models.Interfaces
{
    public interface IOrderRepository
    {
        List<string> OrderDates { get; set; }

        List<string> GetDates();

        List<int> GetOrderNumbers(string date);

        void AddOrder(string date, Order newOrder);

        bool DeleteOrder(string date, int orderNumber);

        void UpdateOrder(string date, List<Order> orders);

        List<Order> GetOrdersByDate(string date);

        void MoveOrder(string date, int orderNumber);
    }
}
