﻿namespace FlooringProgram.Models.Enums
{
    public enum StatusType
    {
        Active,
        Moved,
        Deleted
    }
}
