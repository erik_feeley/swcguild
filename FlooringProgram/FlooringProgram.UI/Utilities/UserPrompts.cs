﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlooringProgram.BLL;
using FlooringProgram.Models;

namespace FlooringProgram.UI.Utilities
{
    public static class UserPrompts
    {
        public static int GetIntFromUser(string message, int[] validRange)
        {
            do
            {
                Console.Write(message);
                var userInput = Console.ReadLine();
                int value;
                if (int.TryParse(userInput, out value) && validRange.Contains(value))
                    return value;

                Console.WriteLine("That was not a valid number.");
                PressKeyForContinue();
            } while (true);
        }

        public static decimal GetDecimalFromUser(string message)
        {
            do
            {
                Console.Write(message);
                var userInput = Console.ReadLine();
                decimal value;
                if (decimal.TryParse(userInput, out value) && value >= 0)
                    return value;

                Console.WriteLine("That was not a valid positive decimal value.");
                PressKeyForContinue();
            } while (true);
        }

        public static string GetStringFromUser(string message)
        {
            do
            {
                Console.Write(message);
                var userInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(userInput))
                    return userInput;

                Console.WriteLine("An empty input is not acceptable!.");
                PressKeyForContinue();
            } while (true);
        }

        public static string GetDateStringFromUser(string message)
        {
            DateTime date;
            do
            {
                Console.Write(message);

                var userInput = Console.ReadLine();
                if (DateTime.TryParse(userInput, out date))
                    break;

                Console.WriteLine("That was not a valid date format!");
                PressKeyForContinue();
                Console.Write(message);

            } while (true);
            return $"{date.Date:MMddyyyy}";
        }

        public static int GetOrderNumberFromUser(OrderManager orderManager, string date)
        {
            var validOrders = orderManager.GetValidOrderNumbers(date);
            if (validOrders.Count == 0)
                return 0;

            OrderScreens.DisplayValidOrderNumbers(validOrders);
            var orderNumber = GetIntFromUser("Please enter the number of the order you want: ", validOrders.ToArray());
            return orderNumber;
        }

        public static string GetDateFromUser(OrderManager orderManager)
        {
            var validDates = orderManager.GetDates();
            OrderScreens.DisplayValidDates(validDates);
            var date = GetDateStringFromUser("\nPlease enter a date: ");
            return date;
        }

        public static Product GetProductFromUser()
        {

            var productManager = new ProductManager();
            var productResponse = productManager.GetProducts();
            var newProduct = GetProductTypeFromUser(productResponse.Data);
            return newProduct;
        }

        public static Tax GetTaxFromUser()
        {
            var taxManager = new TaxManager();
            var taxResponse = taxManager.GetStates();
            var newTaxInfo = GetTaxRateFromUser(taxResponse.Data);
            return newTaxInfo;
        }

        public static string GetYesOrNoFromUser(string message)
        {
            do
            {
                Console.Write(message);
                var userInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(userInput) && (userInput.ToUpper() == "Y" || userInput.ToUpper() == "N"))
                    return userInput;

                Console.WriteLine("\nPlease just enter either (Y)es or (N)o");
                PressKeyForContinue();
            } while (true);
        }

        public static void GetOrderInfoFromUser(Order order)
        {
            order.CustomerName = GetStringFromUser("\nEnter Customer Name: ");
            order.Area = GetDecimalFromUser("Enter Order Area: ");
            Console.WriteLine("\nAll info has been collected generating order\n");
            PressKeyForContinue();
            Console.WriteLine(); // making some space
        }

        public static void PressKeyForContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public static void GetOrderInfoFromUserAllowEmptyStrings(Order order)
        {
            Console.Write($"\nEnter Customer Name({order.CustomerName}): ");
            var userInput = Console.ReadLine();
            order.CustomerName = string.IsNullOrEmpty(userInput) ? order.CustomerName : userInput;
            Console.Write($"Enter Order Area ({order.Area}): ");
            userInput = Console.ReadLine();
            order.Area = string.IsNullOrEmpty(userInput) ? order.Area : decimal.Parse(userInput);
        }

        public static Tax GetTaxRateFromUser(List<Tax> taxes) 
        {
            TaxScreens.DisplayOptions(taxes);
            var userChoice = GetIntFromUser("\nWhich state: ", new[] {1, 2, 3, 4});
            switch (userChoice)
            {
                case 1:
                    return taxes[0];
                case 2:
                    return taxes[1];
                case 3:
                    return taxes[2];
                default:
                    return taxes[3];
            }
        }

        public static Product GetProductTypeFromUser(List<Product> products)
        {
            ProductScreens.DisplayOptions(products);
            var userChoice = GetIntFromUser("\nWhich Product do you want: ", new[] { 1, 2, 3, 4 });
            switch (userChoice)
            {
                case 1:
                    return products[0];
                case 2:
                    return products[1];
                case 3:
                    return products[2];
                default:
                    return products[3];
            }
        }

        public static string DoesDateNeedChange(string date)
        {
            Console.WriteLine("\nIf you change the date it will be moved in the database");
            Console.WriteLine("If the date entered does not match a current active date a new entry will be created.\n");
            var orderManager = new OrderManager();
            var userInput = GetDateFromUser(orderManager);
            date = string.IsNullOrEmpty(userInput) ? date : userInput;
            return date;
        }
    }
}
