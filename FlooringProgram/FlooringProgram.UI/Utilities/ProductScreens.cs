﻿using System;
using System.Collections.Generic;
using FlooringProgram.Models;

namespace FlooringProgram.UI.Utilities
{
    public static class ProductScreens
    {
        public static void DisplayOptions(List<Product> products)
        {
            Console.WriteLine("\nProduct Options");
            Console.WriteLine("------------------------------");
            var count = 0;
            foreach (var product in products)
            {
                count++;
                Console.WriteLine($"{count} - {product.ProductType}: Cost per Square Foot {product.CostPerSquareFoot:c}, Labor Cost per Square Foot: {product.LaborCostPerSquareFoot:c}");
            }
        }
    }
}
