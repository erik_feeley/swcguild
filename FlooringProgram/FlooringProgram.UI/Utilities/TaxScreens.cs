﻿using System;
using System.Collections.Generic;
using FlooringProgram.Models;

namespace FlooringProgram.UI.Utilities
{
    public static class TaxScreens
    {
        public static void DisplayOptions(List<Tax> taxes)
        {
            Console.WriteLine("\nState Options");
            Console.WriteLine("------------------------------");
            var count = 0;
            foreach (var tax in taxes)
            {
                count++;
                Console.WriteLine($"{count} - {tax.StateAbbreviation}: {tax.StateName} - {tax.TaxRate:P}");
            }
        }
    }
}
