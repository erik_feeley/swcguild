﻿using System;
using System.Collections.Generic;
using FlooringProgram.Models;

namespace FlooringProgram.UI.Utilities
{
    public static class OrderScreens
    {
        public static void DisplayMainMenu()
        {
            Console.WriteLine("=====================================\n");
            Console.WriteLine("\tFLOORING PROGRAM\n");
            Console.WriteLine("=====================================\n");
            Console.WriteLine("1. Display Orders");
            Console.WriteLine("2. Add an Order");
            Console.WriteLine("3. Edit an Order");
            Console.WriteLine("4. Remove an Order");
            Console.WriteLine("5. Quit");
        }

        public static void DisplayMainMenuOption(string option)
        {
            Console.WriteLine("\n------------------------------");
            Console.WriteLine($"\n{option}\n");
            Console.WriteLine("------------------------------\n");
        }

        public static void DisplayOrderInformation(Order order)
        {
            Console.WriteLine($"Order Number: {order.OrderNumber}");
            Console.WriteLine($"\tCustomer Name: {order.CustomerName}");
            Console.WriteLine($"\tState: {order.State}");
            Console.WriteLine($"\tTax Rate: {order.TaxRate:P}");
            Console.WriteLine($"\tProduct Type: {order.ProductType}");
            Console.WriteLine($"\tArea: {order.Area}");
            Console.WriteLine($"\tCost per Square Foot: {order.CostPerSquareFoot:c}");
            Console.WriteLine($"\tLabor Cost per Square Foot: {order.LaborCostPerSquareFoot:c}");
            Console.WriteLine($"\tMaterial Cost: {order.MaterialCost:c}");
            Console.WriteLine($"\tLabor Cost: {order.LaborCost:c}");
            Console.WriteLine($"\tTax: {order.Tax:c}");
            Console.WriteLine($"\tTotal: {order.Total:c}\n");
        }

        public static void DisplayValidDates(List<string> dates)
        {
            Console.WriteLine("Current active dates.\n");
            string displayDate;
            foreach (var date in dates)
            {
                displayDate = date;
                displayDate = displayDate.Insert(2, "/");
                displayDate = displayDate.Insert(5, "/");
                Console.WriteLine($"\t{displayDate}");
            }
        }

        public static void DisplayValidOrderNumbers(List<int> orderNumbers)
        {
            Console.WriteLine("\nValid active order numbers\n");
            foreach (var orderNumber in orderNumbers)
            {
                Console.WriteLine($"\t{orderNumber}");
            }
            Console.WriteLine();
        }
    }
}
