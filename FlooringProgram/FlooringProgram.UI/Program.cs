﻿using System;
using FlooringProgram.UI.Workflows;

namespace FlooringProgram.UI
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Title = "Flooring Program";
            Console.WindowHeight = (int)(Console.LargestWindowHeight * .85f);
            Console.WindowWidth = (int)(Console.LargestWindowWidth * .75f);
            Console.SetWindowPosition(0, 0);
            var mainMenuWorkflow = new MainMenuWorkflow();
            mainMenuWorkflow.Execute();
        }
    }
}
