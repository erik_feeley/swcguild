﻿using System;
using FlooringProgram.BLL;
using FlooringProgram.Models.Interfaces;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows.MainMenuOptionWorkflows
{
    public class DeleteOrderWorkflow : IWorkflow
    {
        public void Execute()
        {
            OrderScreens.DisplayMainMenuOption("Remove an Order");

            var orderManager = new OrderManager();
            var date = UserPrompts.GetDateFromUser(orderManager);
            var orderNumber = UserPrompts.GetOrderNumberFromUser(orderManager, date);

            var response = orderManager.DoesOrderExist(date, orderNumber);
            if (!response.Success)
            {
                Console.WriteLine(response.Message);
                UserPrompts.PressKeyForContinue();
                return;
            }

            Console.WriteLine(response.Message);
            OrderScreens.DisplayOrderInformation(response.Data);
            var userInput = UserPrompts.GetYesOrNoFromUser("\nAre you sure you want to delete this file? (Y/N): ");
            if (userInput.ToUpper() == "Y")
            {
                orderManager.DeleteOrder(date, orderNumber);
                Console.WriteLine("Order deleted.");
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                Console.WriteLine("Returning to main menu");
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}
