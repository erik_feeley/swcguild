﻿using System;
using FlooringProgram.BLL;
using FlooringProgram.Models.Interfaces;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows.MainMenuOptionWorkflows
{
    public class DisplayOrdersWorkflow : IWorkflow
    {
        public void Execute()
        {
            OrderScreens.DisplayMainMenuOption("Display Orders");

            var orderManager = new OrderManager();
            var date = UserPrompts.GetDateFromUser(orderManager);
            var response = orderManager.GetOrdersByDate(date);
            if (!response.Success)
            {
                Console.WriteLine(response.Message);
                UserPrompts.PressKeyForContinue();
                return;
            }

            Console.Clear();
            Console.WriteLine(response.Message);
            if (response.Data.Count > 0)
            {
                foreach (var order in response.Data)
                {
                    OrderScreens.DisplayOrderInformation(order);
                }
            }
            else
            {
                Console.WriteLine("No active orders");
            }
            UserPrompts.PressKeyForContinue();
        }
    }
}
