﻿using System;
using FlooringProgram.BLL;
using FlooringProgram.Models;
using FlooringProgram.Models.Enums;
using FlooringProgram.Models.Interfaces;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows.MainMenuOptionWorkflows
{
    public class EditOrderWorkflow : IWorkflow
    {
        public void Execute()
        {
            OrderScreens.DisplayMainMenuOption("Edit Order");

            var orderManager = new OrderManager();
            var date = UserPrompts.GetDateFromUser(orderManager);
            var orderNumber = UserPrompts.GetOrderNumberFromUser(orderManager, date);
            var response = orderManager.DoesOrderExist(date, orderNumber);
            if (!response.Success)
            {
                Console.WriteLine(response.Message);
                UserPrompts.PressKeyForContinue();
                return;
            }

            Console.WriteLine(response.Message);
            UserPrompts.PressKeyForContinue();

            var newDate = GetDateFromUser(orderManager, date, response);
            Console.WriteLine($"\nCurrent product is {response.Data.ProductType}");
            var newProduct = UserPrompts.GetProductFromUser();
            Console.WriteLine($"\nCurrent state is {response.Data.State}");
            var newTaxInfo = UserPrompts.GetTaxFromUser();

            orderManager.MapInfoToOrder(newProduct, newTaxInfo, response.Data);
            UserPrompts.GetOrderInfoFromUserAllowEmptyStrings(response.Data);
            orderManager.UpdateOrder(orderNumber, newDate, response.Data);
            Console.WriteLine("\nOrder has been updated in the database.");
            UserPrompts.PressKeyForContinue();
        }

        private static string GetDateFromUser(OrderManager orderManager, string date, Response<Order> response)
        {
            var newDate = UserPrompts.DoesDateNeedChange(date);
            if (newDate != date)
            {
                response.Data.Status = StatusType.Moved;
                orderManager.MoveOrder(date, response.Data.OrderNumber);
            }

            return newDate;
        }
    }
}
