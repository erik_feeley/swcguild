﻿using System;
using FlooringProgram.BLL;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows.MainMenuOptionWorkflows
{
    public class AddOrderWorkflow : IWorkflow
    {
        public void Execute()
        {
            OrderScreens.DisplayMainMenuOption("Add Order");
            var date = $"{DateTime.Now.Date:MMddyyyy}";
            var product = UserPrompts.GetProductFromUser();
            var tax = UserPrompts.GetTaxFromUser();

            var orderManager = new OrderManager();
            var userChoice = UserPrompts.GetYesOrNoFromUser("\nDo you need to enter a new date (Y)es / (N)o?: ");
            if (userChoice.ToUpper() == "Y")
            {
                date = UserPrompts.GetDateFromUser(orderManager);
            }

            var order = new Order();
            orderManager.MapInfoToOrder(product, tax, order);

            UserPrompts.GetOrderInfoFromUser(order);

            OrderScreens.DisplayOrderInformation(order);
            var commit = UserPrompts.GetYesOrNoFromUser("Are you sure this data is correct? (Y/N): ");
            if (commit.ToUpper() == "N")
            {
                Console.WriteLine("Discarding data and returning to main menu");
                UserPrompts.PressKeyForContinue();
                return;
            }

            var response = orderManager.CreateOrder(date, order);
            Console.WriteLine(response.Message);
            UserPrompts.PressKeyForContinue();
        }
    }
}
