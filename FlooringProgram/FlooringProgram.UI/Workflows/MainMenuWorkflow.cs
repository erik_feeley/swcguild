﻿using System;
using System.Threading;
using FlooringProgram.Models.Interfaces;
using FlooringProgram.UI.Utilities;
using FlooringProgram.UI.Workflows.MainMenuOptionWorkflows;

namespace FlooringProgram.UI.Workflows
{
    public class MainMenuWorkflow : IWorkflow
    {
        public void Execute()
        {
            while (true)
            {
                Console.Clear();
                OrderScreens.DisplayMainMenu();
                var userInput = UserPrompts.GetIntFromUser("\nPlease make your selection: ", new[] { 1, 2, 3, 4, 5 });
                var exit = ProcessChoice(userInput);
                if (exit)
                {
                    Console.WriteLine("\nClosing application");
                    Thread.Sleep(1000);
                    break;
                }  
            }
        }

        private bool ProcessChoice(int userInput)
        {
            IWorkflow workflow = null;

            switch (userInput)
            {
                case 1:
                    workflow = new DisplayOrdersWorkflow();
                    break;
                case 2:
                    workflow = new AddOrderWorkflow();
                    break;
                case 3:
                    workflow = new EditOrderWorkflow();
                    break;
                case 4:
                    workflow = new DeleteOrderWorkflow();
                    break;
                case 5:
                    return true;
            }
            if (workflow != null) workflow.Execute();
            return false;
        }
    }
}
