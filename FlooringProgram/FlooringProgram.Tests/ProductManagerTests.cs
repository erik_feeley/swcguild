﻿using FlooringProgram.BLL;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    public class ProductManagerTests
    {
        [Test]
        public void CanGetProducts()
        {
            var productManager = new ProductManager();
            var response = productManager.GetProducts();
            Assert.AreEqual(true, response.Success);
        }
    }
}
