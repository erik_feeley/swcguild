﻿using FlooringProgram.BLL;
using FlooringProgram.Models;
using NUnit.Framework;

namespace FlooringProgram.Tests //also test the repos!!!!
{
    [TestFixture]
    public class OrderManagerTests
    {
        [Test]
        public void CanLoadAccountsByDate()
        {
            var date = "06012013";
            var orderManager = new OrderManager();

            var response = orderManager.GetOrdersByDate(date);

            Assert.AreEqual(true, response.Success);
        }

        [Test]
        public void LoadAccountsByDateCanFail()
        {
            var date = "99999999";
            var orderManager = new OrderManager();

            var response = orderManager.GetOrdersByDate(date);

            Assert.AreEqual(false, response.Success);
        }

        [Test]
        public void CanCreateOrder()
        {
            var order = new Order
            {
                CustomerName = "testyguy", State = "testystate", TaxRate = .05m, ProductType = "testyproduct", Area = 5m,
                CostPerSquareFoot = 2m, LaborCostPerSquareFoot = 4m
            };
            var orderManager = new OrderManager();
            var response = orderManager.CreateOrder("05102005", order);

            var orders = orderManager.GetOrdersByDate("05102005");
            var loadedOrder = orders.Data.Find(o => o.CustomerName == order.CustomerName);

            Assert.AreEqual(true, response.Success);
            Assert.AreEqual(order.CustomerName, loadedOrder.CustomerName);
        }

        [Test]
        public void CanDeleteOrder()
        {
            var orderManager = new OrderManager();
            var response = orderManager.DeleteOrder("06012013", 1);

            Assert.AreEqual(true, response);
        }

        [Test]
        public void CanMoveOrder()
        {
            var orderManager = new OrderManager();
            var response = orderManager.MoveOrder("06022013", 2);

            Assert.AreEqual(true, response);
        }

        [Test]
        public void DoesOrderExistReturnsExpected()
        {
            var orderManager = new OrderManager();
            var response = orderManager.DoesOrderExist("06012013", 2);
            Assert.AreEqual(true, response.Success);
        }

        [Test]
        public void CanUpdateOrder()
        {
            var orderManager = new OrderManager();
            orderManager.UpdateOrder(2, "06012013", new Order { OrderNumber = 2, CustomerName = "flurtfurp", State = "michigan", TaxRate = 5.75m / 100, ProductType = "wood", Area = 30m, CostPerSquareFoot = 3.50m, LaborCostPerSquareFoot = 4.15m });

            var orders = orderManager.GetOrdersByDate("06012013");
            var orderToCheck = orders.Data.Find(o => o.OrderNumber == 2);

            Assert.AreEqual("wood", orderToCheck.ProductType);
        }

        [Test]
        public void CanAddOrderAfterDeletes()
        {
            var orderManager = new OrderManager();

            orderManager.CreateOrder("01010101", new Order());
            orderManager.CreateOrder("01010101", new Order());

            orderManager.DeleteOrder("01010101", 1);
            orderManager.DeleteOrder("01010101", 2);

            var response = orderManager.CreateOrder("01010101", new Order());

            Assert.AreEqual(true, response.Success);
        }
    }
}
