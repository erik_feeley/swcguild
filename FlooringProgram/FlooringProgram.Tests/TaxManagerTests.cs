﻿using FlooringProgram.BLL;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    public class TaxManagerTests
    {
        [Test]
        public void CanLoadStates()
        {
            var taxManager = new TaxManager();
            var response = taxManager.GetStates();
            Assert.AreEqual(true, response.Success);
        }
    }
}
